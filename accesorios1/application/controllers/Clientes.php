<?php /**
 *
 */
class Clientes extends CI_Controller {
  //contructor de la clase
  public function __construct(){
    //constructor
    parent::__construct();
    // cargamos modelo cliete
    $this->load->model('cliente');
    //proteger las ventanas
    
}
  public function tabla(){
    $data["listadoClientes"]=$this->cliente->obtenerDatos();
    $this->load->view('clientes/tabla',$data);//pasando parametros a la vista


  }
  // /* Renderizar el listado de computadoras*/
  public function index(){
    $data["listadoClientes"]=$this->cliente->obtenerDatos();
    // /* Cargar la viesta index*/
    // /* Carpeta/archivos*/
    $this->load->view('encabezado');
    $this->load->view('clientes/index');//pasando parametros a la vista
    $this->load->view('pie');

  }
  // /* Funcion nuevo*/
  public function nuevo(){
    // /* Cargar la viesta nuevo*/
    // /* Carpeta/archivos*/
    $this->load->view('encabezado');
    $this->load->view('clientes/nuevo');
    $this->load->view('pie');

  }

  public function guardarClientes(){

  $cedula=$this->input->post('cedula_cli');
  $apellidos=$this->input->post('apellidos_cli');
  $nombres=$this->input->post('nombres_cli');
  $direccion=$this->input->post('direccion_cli');
  $telefono=$this->input->post('celular_cli');

  $datosNuevoCliente= array("cedula_cli" =>$cedula ,
  "apellidos_cli"=>$apellidos,
  "nombres_cli"=>$nombres,
  "direccion_cli"=>$direccion,
  "celular_cli"=>$telefono);
  if ($this->cliente->insertar($datosNuevoCliente)) {
    // MENSAJE FLASH PARA CONFIRMAR LA INSERCION DEL CLIENTE
    $this->session->set_flashdata('confirmacion','Cliente inseretado exitosamente');
    redirect('clientes/index');
  }else {
    // no se inserto
    echo "Error al insertar";
  }
}
// metodo para eliminar cliente reciviendo el parametro // Id
public function eliminarCliente($id){
  // llamamos al modelo cliente
  $this->cliente->eliminarPorId($id);
  // validacion de la eliminacion si se realizo o no//
  if ($this->cliente->eliminarPorId($id)) {
    // MENSAJE FLASH PARA CONFIRMAR LA INSERCION DEL CLIENTE
    $this->session->set_flashdata('confirmacion','Cliente eliminado exitosamente');
    redirect("clientes/index");
  }else {
    echo "Error al eliminar";
  }
  }


// funcion para renderizar al la vista editar
public function editar($id){
  $data["clienteEditar"]=$this->cliente->obtenerPorId($id);
  $this->load->view('encabezado');
  // Cargar la vista pasando como parametro data
  $this->load->view('clientes/editar',$data);
  $this->load->view('pie');
}

// metodo para llamar a la actualizacion del imagecolordeallocate
public function actualizarCliente(){
  $id_cli=$this->input->post('id_cli');
  $datosEditados=array(
    "cedula_cli"=>$this->input->post('cedula_cli'),
    "apellidos_cli"=>$this->input->post('apellidos_cli'),
    "nombres_cli"=>$this->input->post('nombres_cli'),
    "direccion_cli"=>$this->input->post('direccion_cli'),
    "celular_cli"=>$this->input->post('celular_cli')
  );
  if ($this->cliente->actualizar($id_cli,$datosEditados)) {
    // MENSAJE FLASH PARA CONFIRMAR LA ACTUALIZACION DEL CLIENTE
    $this->session->set_flashdata('confirmacion','Cliente actualizado exitosamente');
    redirect('clientes/index');
  } else {
    // MENSAJE de erro FLASH PARA LA ACTUALIZACION DEL CLIENTE
    $this->session->set_flashdata('confirmacion','Clienteactualizado ');
  }

}


public function validarCedulaExistente() {
  $cedula_cli=$this->input->post('cedula_cli');
  $clienteExistente=$this->cliente->consultaClientePorCedula($cedula_cli);
  if ($clienteExistente) {
    // print_r sirve para mostrar datos de un array
    echo json_encode(FALSE);
  }else {
    echo json_encode(TRUE);
  }
}

}

 ?>
