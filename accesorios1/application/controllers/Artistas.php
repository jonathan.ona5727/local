<?php /**
 *
 */
class Artistas extends CI_Controller {
  //contructor de la clase
  public function __construct(){
    parent::__construct();//caragmos el contructor del padre
    // cargamos modelo usuario
    $this->load->model('artista');
    $this->load->model('genero');
    //proteger las ventanas

  }
  // /* Renderizar el listado de usuarios*/
  public function index(){
   $data["listadoGeneros"]=$this->genero->obtenerDatos();
    // /* Cargar la viesta index*/
    // /* Carpeta/archivos*/
    $this->load->view('encabezado');
    $this->load->view('artistas/index',$data);//pasando parametros a la vista
    $this->load->view('pie');

  }
  public function tabla(){
    $data["listadoArtistas"]=$this->artista->obtenerTodos();
    // /* Cargar la viesta index*/
    // /* Carpeta/archivos*/
    $this->load->view('encabezado');
    $this->load->view('artistas/tabla',$data);//pasando parametros a la vista
    $this->load->view('pie');

  }
  // /* Funcion nuevo*/
  public function nuevo(){
    $data["listadoGeneros"]=$this->genero->obtenerDatos();
    // /* Cargar la viesta nuevo*/
    // /* Carpeta/archivos*/
    $this->load->view('encabezado');
    $this->load->view('artistas/nuevo',$data);
    $this->load->view('pie');

  }
  public function tablaartistas(){
  $data["listadoGeneros"]=$this->genero->obtenerDatos();
  $data["listadoArtistas"]=$this->artista->obtenerTodos();
  // /* Cargar la viesta nuevo*/
  $this->load->view('artistas/tablaartistas',$data);
}
public function guardarArtistas(){
  // inicio proceso de subida de arcgivos
        $id_imagen=$this->input->post("id_imagen");
      // ruta de subid de archivo
        $config['upload_path']=APPPATH.'../uploads/';
        // tipo de archivo permitido
        $config['allowed_types']='jpeg|jpg|png';
        // definir peso maximo de subida
        $config['max_size']=5*1024;
        // creamos un nombre aleatorio
        $nomnre_aleatorio="Artista_".time()*rand(100,10000);
        // asignando el nombre al archivo subido
        $config['file_name']=$nomnre_aleatorio;
        // Cargando la libreria upload
        $this->load->library('upload',$config);
        if ($this->upload->do_upload("imagen_arti")) {
          // capturando informacion del archivo
          $dataArchivoSubido=$this->upload->data();
          // obteniendo el nombre del archivo
          $nombre_archivo_subido=$dataArchivoSubido["file_name"];
        }else {
          // cuando el archivo no se sube el nombre queda vacio
          $nombre_archivo_subido="";
        }
      // fin proceso de subida de archivos

$foto=$this->input->post('imagen_arti');
$nombre=$this->input->post('nombre_arti');
$telefono=$this->input->post('telefono_arti');
$genero=$this->input->post('fk_id_ge');
$costo=$this->input->post('costo_arti');
$datosNuevaArtista= array("imagen_arti" =>$nombre_archivo_subido,
"nombre_arti" =>$nombre ,
"telefono_arti" =>$telefono ,
"fk_id_ge" =>$genero ,
"costo_arti" =>$costo);
if ($this->artista->insertar($datosNuevaArtista)) {
  // si se inserto
  // MENSAJE FLASH PARA CONFIRMAR LA INSERCIOND DE LA PELICULA
  $this->session->set_flashdata('confirmacion','Artista o grupo insertada correctamente');
  redirect('artistas/index');
}else {
  // no se inserto
  echo "Error al insertar";
}
}
// metodo para eliminar pelicula reciviendo el parametro // Id
public function eliminarArtista($id){
  $data['artistaEditar']=$this->artista->obtenerPorId($id);
  $id_imagen=$data['artistaEditar']->imagen_arti;
  unlink(APPPATH.'../uploads/'.$id_imagen);
  // llamamos al modelo usuarios
  $this->artista->eliminarPorId($id);
  // validacion de la eliminacion si se realizo o no//
  if ($this->artista->eliminarPorId($id)) {
    // mensaje flash de confirmacion para eliminar
    $this->session->set_flashdata('confirmacion','Artista o grupo eliminada exitosamente');
    redirect("artistas/index");
  }else {
    echo "Error al eliminar";
  }
  }


// funcion para renderizar al la vista editar usuarios
public function editar($id){
  $data["listadoGeneros"]=$this->genero->obtenerDatos();
  $data["artistaEditar"]=$this->artista->obtenerPorId($id);
  $this->load->view('encabezado');
  // Cargar la vista pasando como parametro data
  $this->load->view('artistas/editar',$data);
  $this->load->view('pie');
}

// metodo para llamar ala actualizacion de Peliculas
public function actualizarArtista(){
  // inicio proceso de subida de arcgivos
  $id_imagen=$this->input->post("id_imagen");
// ruta de subid de archivo
  $config['upload_path']=APPPATH.'../uploads/';
  // tipo de archivo permitido
  $config['allowed_types']='jpeg|jpg|png';
  // definir peso maximo de subida
  $config['max_size']=5*1024;
  // creamos un nombre aleatorio
  unlink(APPPATH.'../uploads/'.$id_imagen);
  // asignando el nombre al archivo subido
  $config['file_name']=$id_imagen;
  // Cargando la libreria upload
  $this->load->library('upload',$config);
  if ($this->upload->do_upload("imagen_arti")) {
    // capturando informacion del archivo
    $dataArchivoSubido=$this->upload->data();
    // obteniendo el nombre del archivo
    $nombre_archivo_subido=$dataArchivoSubido["file_name"];
  }else {
    // cuando el archivo no se sube el nombre queda vacio
    $nombre_archivo_subido="";
  }
// fin proceso de subida de archivos
  $id_arti=$this->input->post('id_arti');
  $datosEditados=array(
    "imagen_arti"=>$nombre_archivo_subido,
    "nombre_arti"=>$this->input->post('nombre_arti'),
    "telefono_arti"=>$this->input->post('telefono_arti'),
    "fk_id_ge"=>$this->input->post('fk_id_ge'),
    "costo_arti"=>$this->input->post('costo_arti')
  );
  if ($this->artista->actualizar($id_arti,$datosEditados)) {
    // mensaje flash para la actualizacion de pelicula
    $this->session->set_flashdata('confirmacion','Artista o grupo actualizada exitosamente');
    redirect('artistas/index');
  } else {
    echo "Error al actualizar";
  }

}
// nombre exixtente
public function validarArtistaExistente() {
  $nombre_arti=$this->input->post('nombre_arti');
  $clienteExistente=$this->artista->consultaArtistaPorNombre($nombre_arti);
  if ($clienteExistente) {
    // print_r sirve para mostrar datos de un array
    echo json_encode(FALSE);
  }else {
    echo json_encode(TRUE);
  }
}
// telefono clienteExistente
public function validarTelefonoExistente() {
  $telefono_arti=$this->input->post('telefono_arti');
  $clienteExistente=$this->artista->consultaTelefono($telefono_arti);
  if ($clienteExistente) {
    // print_r sirve para mostrar datos de un array
    echo json_encode(FALSE);
  }else {
    echo json_encode(TRUE);
  }
}

}

 ?>
