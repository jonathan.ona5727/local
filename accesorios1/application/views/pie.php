<script>

function confirmation(ev) {
   ev.preventDefault();
   var urlToRedirect = ev.currentTarget.getAttribute('href'); //use currentTarget because the click may be on the nested i tag and not a tag causing the href to be empty
   console.log(urlToRedirect); // verify if this is the right URL
   Swal.fire({
  title: '¿Estas seguro?',
  text: "¡Esto sera permanente!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '¡Borralo!',
  cancelButtonText:'Cancelar'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href = urlToRedirect;
  }
});
}
</script>

<style media="screen">
/* . párallamar clases */
  .error{
    color:red;
    FONT-weight: normal;
  }
  /* marque los cuadros de error  */
  input.error{
    border: 1px solid red
  }
</style>

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-3">

        </div>
        <div class="col-lg-3">

        </div>
        <div class="col-lg-3">

        </div>
        <div class="col-lg-3">

        </div>
        <div class="col-lg-12">
          <div class="about footer-item">
            <div class="logo">
              <a href="#"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="Onix Digital TemplateMo"></a>
            </div>
            <a href="#">info@company.com</a>

          </div>
          <div class="copyright">
            <p>Copyright © 2021 Onix Digital Co., Ltd.
            <br>
            Realizado por: <a rel="nofollow" href="https://templatemo.com" title="free CSS templates">Jonathan Oña</a></p>
          </div>
        </div>
      </div>
    </div>
  </footer>


  <!-- Scripts -->
  <script src="<?php echo base_url(); ?>/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/owl-carousel.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/animation.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/imagesloaded.js"></script>
  <script src="<?php echo base_url(); ?>/assets/js/custom.js"></script>
 <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>
  <script>
  // Acc
    $(document).on("click", ".naccs .menu div", function() {
      var numberIndex = $(this).index();

      if (!$(this).is("active")) {
          $(".naccs .menu div").removeClass("active");
          $(".naccs ul li").removeClass("active");

          $(this).addClass("active");
          $(".naccs ul").find("li:eq(" + numberIndex + ")").addClass("active");

          var listItemHeight = $(".naccs ul")
            .find("li:eq(" + numberIndex + ")")
            .innerHeight();
          $(".naccs ul").height(listItemHeight + "px");
        }
    });
  </script>
</body>
</html>
