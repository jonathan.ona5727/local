<br><br><br>

<section class="">
       <div class="bradcumbContent">
           <h2>Actualizar ACCESORIO</h2>
       </div>

   </section>
   <!-- ##### Breadcumb Area End ##### -->

   <!-- ##### Login Area Start ##### -->
   <section class="login-area section-padding-100">
       <div class="container">
           <div class="row justify-content-center">
               <div class="col-md-8 ">
                   <div class="login-content">
    <form class="" enctype="multipart/form-data" action="<?php echo site_url()?>/artistas/actualizarArtista"
    method="post" id="frm_editar_artista">

    <input type="hidden" name="id_arti"  id="id_arti" class="form-control"
    value="<?php echo $artistaEditar->id_arti;?>">
    <input type="hidden" name="id_imagen"  id="id_imagen" class="form-control-input"
    value="<?php echo $artistaEditar->imagen_arti;?>">

    <div class="row">

      <div class="col-md-4 text-right">
        <label for="">Foto: </label>
      </div>
      <div class="col-md-8 text-left">
        <input type="file" name="imagen_arti"  id="imagen_arti" class="form-control"
        value="<?php echo $artistaEditar->imagen_arti; ?>" placeholder="Ingrese la imagen del artista o grupo" required>
        <br>
      </div>
      <div class="col-md-4 text-right">
        <label for="">Nombre: </label>
      </div>
      <div class="col-md-8 text-left">
        <input type="text" name="nombre_arti"  id="nombre_arti" class="form-control"
        value="<?php echo $artistaEditar->nombre_arti; ?>" placeholder="Ingrese el nombre"required>
        Ej. Maluma- Papaya DADA
      </div>
      <br>

      <div class="col-md-4 text-right">
        <label for="">Telefono: </label>
      </div>
      <div class="col-md-8 text-left">
        <input type="number" name="telefono_arti"  id="telefono_arti" class="form-control"
        value="<?php echo $artistaEditar->telefono_arti; ?>" placeholder="Ingrese el telefono" required>
        Ej. 235689555
      </div>
      <br>
      <!-- genero -->
      <div class="col-md-4 text-right">
        <label for="">Género Musical: </label>
      </div>
      <div class="col-md-8 text-right">
        <td><select class="form-control" name="fk_id_ge" id="fk_id_ge">
          <option value="">--Seleccione--</option>
          <?php if ($listadoGeneros): ?>
            <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
              <option value="<?php echo $generoTemporal->id_ge; ?>">
                <?php echo $generoTemporal->nombre_gen; ?>
              </option>
            <?php endforeach; ?>
          <?php endif; ?>
        </select></td>
        <br>
      </div>
      <br><br>
      <!-- costo -->
      <div class="col-md-4 text-right">
        <label for="">Tarifa: </label>
      </div>
      <div class="col-md-8 text-left">
        <input type="number" name="costo_arti"  id="costo_arti" class="form-control"
        value="<?php echo $artistaEditar->costo_arti; ?>"  required>
        Ej. $1.000
      </div>
      <br>
    </div>
      <button type="submit" name="button" class="btn btn-primary" style="background-color:#17BB0F">
        Guardar
      </button>
      <a href="<?php echo site_url()?>/artistas/index"  class="btn btn-danger">
        Cancelar
      </a>
      <br><br><br><br><br><br>
    </form>
  </div>
</div>
</div>
</div>
</section>
<!-- ##### Login Area End ##### -->

<script type="text/javascript">
  $("#genero_pel").val('<?php echo $artistaEditar->genero_pel; ?>');
</script>
<script type="text/javascript">
$("#fk_id_ge").val('<?php echo $artistaEditar->fk_id_ge; ?>');

$("#frm_editar_artista").validate({
  rules:{
    imagen_arti:{
      required:true
    },
    nombre_arti:{
      required:true,
    },
    telefono_arti:{
      required:true,
      digits:true
    },
    costo_arti:{
      required:true,
      digits:true
    }
  },
  // -----------------Mensajes----------
  messages:{
    imagen_arti:{
      required:"Por favor llene este campo"
    },
    nombre_arti:{
      required:"Por favor ingrese el nombre"
    },
    telefono_arti:{
      required:"Por favor ingrese el telefono",
      digits:"Por favor ingrese numeros"
    },
    costo_arti:{
      required:"Por favor ingrese la tarifa",
      digits:"Por favor ingrese numeros"
    }
      }
    });


</script>
