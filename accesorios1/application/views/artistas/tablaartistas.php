<center>
  <h4 style="font-weight:bold">Accesorio Registrado</h4>
</center>
<?php if ($listadoArtistas):?>
  <table class="table table-bordered table-striped  table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">FOTO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">CANTIDAD</th>
        <th class="text-center">ACCESORIO<br>COMERCIALL</th>
        <th class="text-center">PRECIO</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoArtistas->result() as $artistaTemporal ): ?>
        <tr>
          <td class="text-center"><?php echo $artistaTemporal->id_arti; ?></td>
          <td class="text-center">
          <?php if ($artistaTemporal->imagen_arti!=""): ?>
            <a target="_blank"
              href="<?php echo base_url('uploads').'/'.$artistaTemporal->imagen_arti;?>">
              <img src="<?php echo base_url('uploads').'/'.$artistaTemporal->imagen_arti;?>"
              width="80px" alt="">
              </a>
            <?php else: ?>
              N/A
            <?php endif; ?>

          <td class="text-center"><?php echo $artistaTemporal->nombre_arti; ?></td>
          <td class="text-center"><?php echo $artistaTemporal->telefono_arti; ?></td>
          <td class="text-center"><?php echo $artistaTemporal->nombre_gen; ?></td>
          <td class="text-center"><?php echo $artistaTemporal->costo_arti; ?></td>
          <td class="text-center">
            <a  href="<?php echo site_url();?>/artistas/editar/<?php echo $artistaTemporal->id_arti;?>"><i class='fa fa-pencil' title="Editar"></i></a>
            <!-- onclick="return confirm('Estas seguro de eliminar es para que salga un mensaje de configuracion -->
            <a href="<?php echo site_url();?>/artistas/eliminarArtista/<?php echo $artistaTemporal->id_arti;?>"
              onclick="confirmation(event)">
              <i class='fa fa-trash-o' title="Eliminar"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    No se encontraron artistas registrados
  </div>
<?php endif; ?>
