<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <title>Onix Digital Marketing HTML5 Template</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/templatemo-onix-digital.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/animated.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.css">


  <link rel="stylesheet" href="//cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
  <script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js">

  </script>


          <?php if ($this->session->flashdata('confirmacion')):?>
    	<script type="text/javascript">
    	$(document).ready(function(){
    		Swal.fire(
    		  'CONFIRMACION!',
    			'<?php echo
    			$this->session->flashdata('confirmacion');?>',

    			'success'
    		//  'warning'//icono de advertencia

    		);
    	});
    	</script>
    <?php endif; ?>


          <?php if ($this->session->flashdata('eliminacion')):?>
          <script type="text/javascript">
          Swal.fire({
          	title: 'Are you sure?',
          	text: "You won't be able to revert this!",
          	icon: 'warning',
          	showCancelButton: true,
          	confirmButtonColor: '#3085d6',
          	cancelButtonColor: '#d33',
          	confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
          	if (result.isConfirmed) {
          		Swal.fire(
          			'Deleted!',
          			'Your file has been deleted.',
          			'success'
          		)
          	}
          })

          </script>
          <?php endif; ?>
          <?php if ($this->session->flashdata('error')):?>
  		<script type="text/javascript">
  		$(document).ready(function(){
  			Swal.fire(
  				'ERROR!',
  				'<?php echo
  				$this->session->flashdata('error');?>',
  				'error'

  			);
  		});
  		</script>
  	<?php endif; ?>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

  </head>

<body>

  <!-- ***** Preloader Start ***** -->
  <div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
      <span class="dot"></span>
      <div class="dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- ***** Preloader End ***** -->

  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">
    <div class="container">
      <div class="row">

        <div class="col-12">
          <nav class="main-nav">
            <!-- ***** Logo Start ***** -->
            <a href="<?php echo base_url(); ?>" class="logo">
              <img src="<?php echo base_url(); ?>/assets/images/logo.png">
            </a>
            <!-- ***** Logo End ***** -->
            <!-- ***** Menu Start ***** -->
            <ul class="nav">
              <li class="scroll-to-section"><a href="<?php echo site_url(); ?>" class="active">Inicio</a></li>
              <li class="scroll-to-section"><a href="<?php echo site_url(); ?>/gestiones/gestionClientes">Clientes</a></li>
              <li class="scroll-to-section"><a href="<?php echo site_url(); ?>/artistas/index">Accesorios</a></li>
              <li class="scroll-to-section"><a href="<?php echo site_url(); ?>/generos/index">Categoria</a></li>
              <li class="scroll-to-section"><a href="<?php echo site_url(); ?>/reportes/index">Reportes</a></li>
              <li class="scroll-to-section"><a href="<?php echo site_url(); ?>/contratos/index">Adquirir Accesorio</a></li>
              <li class="scroll-to-section"><div class="main-red-button-hover"><a href="<?php echo site_url(); ?>/seguridades/loging">Contact Us Now</a></div></li>
            </ul>
            <a class='menu-trigger'>
                <span>Menu</span>
            </a>
            <!-- ***** Menu End ***** -->
          </nav>
        </div>
      </div>
    </div>
  </header>
  <meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://code.jquery.com/jquery-3.6.0.js"integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
<!-- jsdelivr es un cdn para los mensajes de confirmacion -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>/assets\librerias\bootstrap 3/css/bootstrap.css"> -->
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>/assets\librerias\bootstrap 3/js/bootstrap.js"></script> -->

<!-- Funcion para confirmar los procesos -->
