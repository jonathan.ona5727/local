<center><h3><font color="black"> CLIENTE REGISTRADO</font></h3></center><br>

<?php if ($listadoClientes):?>
<table class="table table-bordered table-striped  table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">CEDULA</th>
      <th class="text-center">APELLIDOS</th>
      <th class="text-center">NOMBRES</th>
      <th class="text-center">DIRECCIÓN</th>
      <th class="text-center">TELÉFONO CELULAR</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoClientes->result() as $usuarioTemporal ): ?>
      <tr>
        <td class="text-center"><?php echo $usuarioTemporal->id_cli; ?></td>
        <td class="text-center"><?php echo $usuarioTemporal->cedula_cli; ?></td>
        <td class="text-center"><?php echo $usuarioTemporal->apellidos_cli; ?></td>
        <td class="text-center"><?php echo $usuarioTemporal->nombres_cli; ?></td>
        <td class="text-center"><?php echo $usuarioTemporal->direccion_cli; ?></td>
        <td class="text-center"><?php echo $usuarioTemporal->celular_cli; ?></td>

        <td class="text-center">
          <a href="<?php echo site_url();?>/clientes/editar/<?php echo $usuarioTemporal->id_cli;?>"><i class='fa fa-pencil' title="Editar"></i></a>
          <!-- onclick="return confirm('Estas seguro de eliminar es para que salga un mensaje de configuracion -->
          <a href="<?php echo site_url();?>/clientes/eliminarCliente/<?php echo $usuarioTemporal->id_cli;?>"
               onclick="confirmation(event)">
               <i class='fa fa-trash-o' title="Eliminar"></i>
          </a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php else: ?>
<div class="alert alert-danger">
  No se encontraron usuarios registrados
</div>
<?php endif; ?>
