<section class="breadcumb-area bg-img bg-overlay" style="background-image: url(https://images2.alphacoders.com/215/thumb-1920-215912.jpg);">
      <title>One Music</title>

      <!-- Favicon -->
      <link rel="icon" href="<?php echo base_url(); ?>/assets\img/core-img/favicon.ico">

      <!-- Stylesheet -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>/assets\style.css">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <script src="https://code.jquery.com/jquery-3.6.0.js"integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="crossorigin="anonymous"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
  <!-- jsdelivr es un cdn para los mensajes de confirmacion -->
      <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <div class="bradcumbContent">

            <h2>FACTURA</h2>
        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->

   <!-- ##### Login Area Start ##### -->
   <section class="login-area section-padding-100">
       <div class="container">
           <div class="row justify-content-center">
             <div class="col-md-2">

             </div>
               <div class="col-md-8 ">
                 <div class="col-3">

                 </div>
                   <div class="login-content">
                     <form class=""  id="fmContrato"method="post" action="<?php echo site_url()?>/contratos/actualizarcontrato">
                     <div class="row">

                       <div class="col-md-4">
                         <label for="">Cliente</label>
                       </div>
                       <div class="col-md-8">
                         <select class="form-control" name="fk_id_cli" id="fk_id_cli" disabled>
                           <option value="">Seleccione--</option>
                           <?php if ($listadoClientes): ?>
                             <?php foreach ($listadoClientes->result() as $key => $clienteTemporal): ?>
                               <option value="<?php echo $clienteTemporal->id_cli; ?>">
                                 <?php echo $clienteTemporal->cedula_cli; ?>
                                 <?php echo $clienteTemporal->apellidos_cli; ?>
                                 <?php echo $clienteTemporal->nombres_cli; ?>
                               </option>
                             <?php endforeach; ?>
                           <?php endif; ?>

                         </select>
                         <br>
                       </div>
                       <div class="col-md-4">
                         <label for="">Artista </label>
                         </div>
                         <div class="col-md-8">
                         <select class="form-control" name="fk_id_arti" id="fk_id_arti" disabled>
                           <option value="">Seleccione--</option>
                           <?php if ($listadoartistas): ?>
                             <?php foreach ($listadoartistas->result() as $key => $artistaTemporal): ?>
                               <option value="<?php echo $artistaTemporal->id_arti; ?>">
                                 <?php echo $artistaTemporal->nombre_arti; ?>
                               $  <?php echo $artistaTemporal->costo_arti; ?>
                               </option>
                             <?php endforeach; ?>
                           <?php endif; ?>
                         </select>
                         <br>
                       </div>
                       <div class="col-md-4">
                         <label for="">Fecha Inicio</label>
                       </div>
                       <div class="col-md-8">
                         <input type="date" name="fecha_contrato_con" id="fecha_contrato_con" value="<?php echo $contratoEditar->fecha_contrato_con; ?>" disabled class="form-control">
                         <br>
                       </div>
                       <div class="col-md-4">
                         <label for="">Fecha Fin</label>
                       </div>
                       <div class="col-md-8">
                         <input type="date" name="fecha_pago_con" id="fecha_pago_con" value="<?php echo $contratoEditar->fecha_pago_con; ?>" disabled class="form-control">
                         <br>
                      </div>
  <div class="col-md-12 text-center">
    <button type="button" name="button" class="btn btn-primary" style="background-color:#17BB0F" onclick="imprimir();">
      Imprimir
    </button>
    <a href="<?php echo site_url()?>/contratos/index"  class="btn btn-danger">
      Regresar
    </a>
  </div>

</div>
</form>
</div>
<div class="col-md-3 text-center" >
</div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
// Colocamos en el imput select los valores que tomamos de la cadena alquilerEditar con el id que proporciona el usuario
$("#fk_id_cli").val('<?php echo $contratoEditar->fk_id_cli;?>');
$("#fk_id_arti").val('<?php echo $contratoEditar->fk_id_arti;?>');

</script>
<iframe id="if_print" style="display:none">

</iframe>

<script  type="text/javascript">
  function imprimir(){
    document.getElementById('if_print').src = '<?php echo site_url("facturas/imprimir"); ?>';
    print();
  }

</script>
