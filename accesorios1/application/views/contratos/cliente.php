<!-- ##### Breadcumb Area Start ##### -->
    <section class="breadcumb-area bg-img bg-overlay" style="background-image: url(https://images8.alphacoders.com/478/thumb-1920-478521.jpg);">
        <div class="bradcumbContent">
            <h2>Contrato</h2>
        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->

   <!-- ##### Login Area Start ##### -->
   <section class="login-area section-padding-100">
       <div class="container">
           <div class="row justify-content-center">
             <div class="col-md-2">

             </div>
               <div class="col-md-8 ">
                 <div class="col-3">

                 </div>
                   <div class="login-content">
                     <form class=""  id="fmContrato"method="post" action="<?php echo site_url()?>/contratos/guardarPedido">
                     <div class="row">

                       <div class="col-md-4">
                         <label for="">Cliente</label>
                       </div>
                       <div class="col-md-8">
                         <select class="form-control" name="fk_id_cli" id="fk_id_cli" required>
                           <option value="">Seleccione--</option>
                           <?php if ($listadoClientes): ?>
                             <?php foreach ($listadoClientes->result() as $key => $clienteTemporal): ?>
                               <option value="<?php echo $clienteTemporal->id_cli; ?>">
                                 <?php echo $clienteTemporal->cedula_cli; ?>
                                 <?php echo $clienteTemporal->apellidos_cli; ?>
                                 <?php echo $clienteTemporal->nombres_cli; ?>
                               </option>
                             <?php endforeach; ?>
                           <?php endif; ?>

                         </select>
                         <br>
                       </div>
                       <div class="col-md-4">
                         <label for="">artistas</label>
                         </div>
                         <div class="col-md-8">
                         <select class="form-control" name="fk_id_arti" id="fk_id_arti" required>
                           <option value="">Seleccione--</option>
                           <?php if ($listadoartistas): ?>
                             <?php foreach ($listadoartistas->result() as $key => $artistaTemporal): ?>
                               <option value="<?php echo $artistaTemporal->id_arti; ?>">
                                 <?php echo $artistaTemporal->nombre_arti; ?>
                               $  <?php echo $artistaTemporal->costo_arti; ?>
                               </option>
                             <?php endforeach; ?>
                           <?php endif; ?>
                         </select>
                         <br>
                       </div>
                       <div class="col-md-4">
                         <label for="">Fecha actual</label>
                       </div>
                       <div class="col-md-8">
                         <input type="date" name="fecha_contrato_con" id="fecha_contrato_con"  required class="form-control">
                         <br>
                       </div>
                       <div class="col-md-4">
                         <label for="">Fecha depago</label>
                       </div>
                       <div class="col-md-8">
                         <input type="date" name="fecha_pago_con" id="fecha_pago_con" required class="form-control">
                         <br>
                      </div>
                      <input type="hidden" name="estado_con" id="estado_con" name="" value="Pendiente">

  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-primary">Contratar</button>
    &nbsp&nbsp&nbsp&nbsp
    <a href="<?php echo site_url(); ?>/artistas/tabla" class="btn btn-danger">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCancelar&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a>

  </div>

</div>
</form>
</div>
<div class="col-md-3 text-center" >
</div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
// Colocamos en el imput select los valores que tomamos de la cadena alquilerEditar con el id que proporciona el usuario
$("#fk_id_cli").val('<?php echo $contratoEditar->fk_id_cli;?>');
$("#fk_id_arti").val('<?php echo $contratoEditar->fk_id_arti;?>');
$("#estado_con").val('<?php echo $contratoEditar->estado_con;?>');

</script>
<script type="text/javascript">
$("#formulario").validate({
  rules:{
    fk_id_cli:{
      required:true
    },
    apellidos_cli:{
      required:true
    },
    nombres_cli:{
      required:true
    },
    telefono_cli:{
      digits:true,
      minlength:9,
      maxlength:9
    },
    celular_cli:{
      required:true,
      digits:true,
      minlength:10,
      maxlength:10
    },
    direccion_cli:{
      required:true
    }
  },
  // -----------------Mensajes----------
  messages:{
        fk_id_cli:{
          required:"Por favor ingrese un valor"
        },
        apellidos_cli:{
          required:"Por favor ingrese el Apellido"
        },
        nombres_cli:{
          required:"Por favor ingrese los nombres"
        },
        telefono_cli:{
          digits:"Por favor ingrese el telefono ",
          minlength:"El telefono debe tener minimo 9 digitos",
          maxlength:"El telefono debe tener maximo 9 digitos"
        },
        celular_cli:{
          required:"Ingrese el numero de celura",
          digits:"Ingrese solo numeros",
          minlength:"El numero de celular debe tener minimo 10 digitos",
          maxlength:"El numero de celular debe tener maximo 10 digitax"
        },
        direccion_cli:{
          required:"Por favor ingrese la direccion"
        }
      }
    });

</script>
