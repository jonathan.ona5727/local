<br><br><br><br>
<section class="">
       <div class="bradcumbContent">
           <p></p>
           <center><h2>Lista de compras</h2>
       </div>
   </section>
   <!-- ##### Breadcumb Area End ##### -->
   <!-- ##### Login Area Start ##### -->
<div class="container" >
  <div class="row">
    <div class="col-md-12 text-center">
      <br>
      <button type="button" name="submit" class="btn btn-primary" onclick="cargarTabla();">
        Actializar datos
      </button>
      <br>
      <div id="contenedor_tabla">
      </div>
    </div>
  </div>
</div>




<!-- Trigger the modal with a button -->
<div class="col-md-12" align="center">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalContrato"  onclick="abrirModal">AGREGAR COOMPRA</button>
<br><br>
</div>

<!-- Modal -->
<div id="modalContrato" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <h4 class="modal-title"> <b>NUEVA COMPRA </b></h4>
      </div>
      <div class="modal-body">
        <!-- inicio del formulario -->
        <form class=""  action="<?php echo site_url()?>/contratos/guardarcontratos"
          method="post" id="fmContrato" >
        <div class="row">

          <div class="col-md-4">
            <label for="">Cliente</label>
          </div>
          <div class="col-md-8">
            <select class="form-control" name="fk_id_cli" id="fk_id_cli" required>
              <option value="">Seleccione--</option>
              <?php if ($listadoClientes): ?>
                <?php foreach ($listadoClientes->result() as $key => $clienteTemporal): ?>
                  <option value="<?php echo $clienteTemporal->id_cli; ?>">
                    <?php echo $clienteTemporal->cedula_cli; ?>
                    <?php echo $clienteTemporal->apellidos_cli; ?>
                    <?php echo $clienteTemporal->nombres_cli; ?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>

            </select>
            <br>
          </div>
          <div class="col-md-4">
            <label for="">accesorios</label>
            </div>
            <div class="col-md-8">
            <select class="form-control" name="fk_id_arti" id="fk_id_arti" required>
              <option value="">Seleccione--</option>
              <?php if ($listadoartistas): ?>
                <?php foreach ($listadoartistas->result() as $key => $artistaTemporal): ?>
                  <option value="<?php echo $artistaTemporal->id_arti; ?>">
                    <?php echo $artistaTemporal->nombre_arti; ?>
                  $  <?php echo $artistaTemporal->costo_arti; ?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
            </select>
            <br>
            <input type="hidden" name="tarifa_con"  id="tarifa_con" class="form-control"
            value="<?php echo $artistaTemporal->costo_arti; ?>" >
          </div>
          <div class="col-md-4">
            <label for="">Fecha de Compra</label>
          </div>
          <div class="col-md-8">
            <input type="date" name="fecha_contrato_con" id="fecha_contrato_con" value="" required class="form-control" min="<?php echo date('Y-m-d');?>">
            <br>
          </div>
          <div class="col-md-4">
            <label for="">Fecha de Pago</label>
          </div>
          <div class="col-md-8">
            <input type="date" name="fecha_pago_con" id="fecha_pago_con" value="" required class="form-control" min="<?php echo date('Y-m-d');?>">
          <br>
        </div>
          <div class="col-md-4">
            <label for="">Estado</label>
          </div>
          <div class="col-md-8">
            <select class="form-control" name="estado_con" id="estado_con" >
              <option value="">--Seleccione--</option>
              <option value="Pendiente">Pendiente</option>
              <option value="Concluido">Pagado</option>

            </select>
            <br>
         </div>
          </div>

      <!-- comineza el pie dl modal -->
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6">
            <button type="submit" name="button" class="btn btn-success"  >Guardar Compra</button>

          </div>
          <div class="col-md-6">
            <button type="button" name="button" class="btn btn-danger" onclick="cerrarModal();">Cancelar Accion</button>
          </div>
        </div>
      </div>
      <!-- FIN DEL PIE DEL MODAL -->
      </form>
      <!-- fin del formulario -->
    </div>

  </div>
</div>
</div>


<script type="text/javascript">
function cargarTabla(){
  $("#contenedor_tabla").load('<?php echo site_url("contratos/tabla"); ?>');

}
cargarTabla();
</script>

<script type="text/javascript">
    function abrirModal(){
      $("#modalContrato").modal("show");
    }
</script>
<script type="text/javascript">
    function cerrarModal(){
      $("#modalContrato").modal("hide");
    }
</script>



<br><br><br><br>
